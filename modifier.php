<?php 
require ("config/database.php");
if (isset($_POST['modifier'])) {
    if(!empty($_POST['nom'])){
        $req = $db->prepare('UPDATE crud set nom = ? where id = ?');
        $req->execute(array($_POST['nom'], $_GET['id']));
        header('Location: index.php');
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
</head>
<body>
    <div class="container">
    <h1>Modification du nom</h1>
        <form action="" method="post">
            <label for="nom">Nom</label>
            <input type="text" class="form-control" name="nom" id="nom" value="<?php
            $requete = $db->prepare("SELECT * from crud where id = ?");
            $requete->execute(array($_GET['id']));
            $result = $requete->fetchAll();
            foreach ($result as $resu) {
                echo $resu['nom'];
            }
        ?>">
        <br><br>
        <button type="submit" class="btn btn-info" name="modifier">Modifier</button>
        </form>
        
    </div>
</body>
</html>