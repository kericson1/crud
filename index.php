<?php
require ("config/database.php");
require'views/index.view.php';

if(isset($_POST['enregistrer'])){
    if (!empty($_POST['nom'])) {
        $req = $db->prepare('INSERT INTO crud (nom) values(?)');
        $execut = $req->execute(array($_POST['nom']));
        if($execut){
            echo "<p class='alert alert-success'>Merci</p>";
            header('Location: index.php');
        }
    }
    else{
        echo "<p class='alert alert-danger'>Vous n'avez pas renseigner le champ ! </p>";
    }
}