<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
</head>
<body>
    <div class="container">
    <h1>Welcome to my website (I am <b>Kericson</b>)</h1>
        <form action="" method="post">
            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" name="nom" id="nom">
            </div>
            <button type="submit" class="btn btn-info" name="enregistrer">Enregistrer</button>
        </form>
        <br><br><br>
        <div>
            <h4 class="alert alert-dark">La liste des personnes enregistrées</h4>
            <p>
                <?php 
                    $req = $db->query("select * from crud");
                    $execut = $req->fetchall();
                    echo "<table class='table'><thead><th>Nom</th><th>Afficher</th><th>Modifier</th><th>Supprimer</th></thead>";
                    foreach ($execut as $data) {
                        echo "<tbody><td>".$data['nom']."</td><td><a href='afficher.php?id=".$data['id']."' class='btn btn-primary'>Afficher</a></td><td><a href='modifier.php?id=".$data['id']."' class='btn btn-info'>Modifier</a></td><td><a href='supprimer.php?id=".$data['id']."' class='btn btn-danger'>Supprimer</a></td>";
                    }
                    echo "</tbody></table>";
                ?>
            </p>
        </div>
    </div>
    <script src="script.js"></script>
</body>
</html>